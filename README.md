# A Day of Git

## A practical guide on how to work with Git

This guide will show the used Git commands you will most often use in your daily work. 
    
## Get Started

[**Ready? Then start here!**](https://gitlab.com/git-adventures/a-day-of-git/wikis/home)
